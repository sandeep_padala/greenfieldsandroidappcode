package com.greenfields.homes.login;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.greenfields.homes.R;
import com.greenfields.homes.common.Style;
import com.greenfields.homes.common.Utils;
import com.greenfields.homes.fcm.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ShareRideActivity extends AppCompatActivity {

    Button btnShareRide;
    EditText edPickup;
    EditText edDrop;
    EditText edTime;
    String selectedTime;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private DatabaseReference mFirebaseUsersInstance;

    final private String FCM_API = "https://fcm.googleapis.com/fcm/send";
    final private String serverKey = "key=" + "AAAAMQKhJBo:APA91bGSpsJ-L0vd6vQ6naJIZLlamUY5ZbAB_meopLOI5MahCfgWBg6rLTNnZhXKXHES5GzWZka2Rv8_w8-0ek4bncYxxMUIpM9DNWl3znvXEWCYCkflk8p8pKYKBohybSWdN82oL7GH";
    final private String contentType = "application/json";
    final String TAG = "NOTIFICATION TAG";

    String NOTIFICATION_TITLE;
    String NOTIFICATION_MESSAGE;
    String NOTIFICATION_ID;
    String TOPIC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_ride);
        iniviews();
    }

    private void iniviews() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("ridedetails");
        mFirebaseUsersInstance=mFirebaseInstance.getReference("users");
        btnShareRide=findViewById(R.id.btn_share_ride);
        edDrop=findViewById(R.id.drop);
        edPickup=findViewById(R.id.pickup);
        edTime=findViewById(R.id.time);


        edTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
               int  mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(ShareRideActivity.this,R.style.TimePickerTheme,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                selectedTime=Utils.getSelectedTime(hourOfDay,minute);
                                edTime.setText(selectedTime);

                            }
                        }, mHour, mMinute, false);

                timePickerDialog.show();
            }
        });



        btnShareRide.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String drop=edDrop.getText().toString();
                String pick=edPickup.getText().toString();
                if(!Utils.isOnline(ShareRideActivity.this))
                {
                    Utils.showCrototn(ShareRideActivity.this, getResources().getString(R.string.msg_no_network_wait), Style.ALERT);
                    return;
                }


                if (TextUtils.isEmpty(pick)) {
                    Toast.makeText(getApplicationContext(), "Enter pickup address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(drop)) {
                    Toast.makeText(getApplicationContext(), "Enter drop address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(selectedTime)) {
                    Toast.makeText(getApplicationContext(), "Select ride start time!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (Utils.isPastTime(selectedTime)) {
                    Toast.makeText(getApplicationContext(), "Please select future time..!", Toast.LENGTH_SHORT).show();
                    return;
                }
                saveRideDetails();

            }
        });
    }

    private void saveRideDetails() {

        mFirebaseUsersInstance.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               Users data = dataSnapshot.getValue(Users.class);
                RideDetails details=new RideDetails();
                details.DisplayName=data.name+" "+data.plot;
                details.Drop=edDrop.getText().toString();
                details.Pick=edPickup.getText().toString();
                details.startTime=selectedTime;
                details.phone=data.phone;
                details.uid=FirebaseAuth.getInstance().getCurrentUser().getUid();
                String todayDate= Utils.getTodayDate();
                mFirebaseDatabase.child(todayDate).child(System.currentTimeMillis()+"").setValue(details);
                Toast.makeText(getApplicationContext(), "Ride Posted Successfully!", Toast.LENGTH_SHORT).show();
                NOTIFICATION_ID=details.uid;
                SendRideNotification(details.DisplayName);
                finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private void SendRideNotification(String displayName) {

        TOPIC = "/topics/userABC"; //topic has to match what the receiver subscribed to
        NOTIFICATION_TITLE = "Ride info";
        NOTIFICATION_MESSAGE = "New ride created from "+displayName;


        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", NOTIFICATION_TITLE);
            notifcationBody.put("message", NOTIFICATION_MESSAGE);
            notifcationBody.put("uid", NOTIFICATION_ID);

            notification.put("to", TOPIC);
            notification.put("data", notifcationBody);

        } catch (JSONException e) {
            Log.e(TAG, "onCreate: " + e.getMessage() );
        }
        sendNotification(notification);
    }

    private void sendNotification(JSONObject notification) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(ShareRideActivity.this, "Request error", Toast.LENGTH_LONG).show();
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


}
