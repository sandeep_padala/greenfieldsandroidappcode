package com.greenfields.homes.login;

public interface OnPhoneCallClickListener {

    public void performCall(String phone);
}
