package com.greenfields.homes.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.greenfields.homes.R;


/**
 * Created by sandeep padala on 2/9/2018.
 */

public class ServicesFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.services_layout, container, false);

        initViews();
        return view;
    }



    private void initViews() {


    }



}
