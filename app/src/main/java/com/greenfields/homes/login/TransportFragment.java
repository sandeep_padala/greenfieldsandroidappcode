package com.greenfields.homes.login;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.greenfields.homes.R;
import com.greenfields.homes.common.Style;
import com.greenfields.homes.common.Utils;


/**
 * Created by sandeep padala on 2/9/2018.
 */

public class TransportFragment extends Fragment {

    Button btnAskRide,btnShareRide;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.transport_layout, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        initViews();
        super.onActivityCreated(savedInstanceState);
    }

    private void initViews() {
        btnAskRide=getView().findViewById(R.id.btn_share_ride);
        btnShareRide=getView().findViewById(R.id.btn_ask_ride);

        btnShareRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(getActivity()))
                {
                    Intent intent=new Intent(getActivity(),ShareRideActivity.class);
                    startActivity(intent);

                }
                else
                {
                    Utils.showCrototn(getActivity(), getResources().getString(R.string.msg_no_network_wait), Style.ALERT);
                }

            }
        });

        btnAskRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(getActivity()))
                {
                    Intent intent=new Intent(getActivity(),AvailableRideDetailsActivity.class);
                    startActivity(intent);
                }
                else
                {
                    Utils.showCrototn(getActivity(), getResources().getString(R.string.msg_no_network_wait), Style.ALERT);
                }
            }
        });


    }



}
