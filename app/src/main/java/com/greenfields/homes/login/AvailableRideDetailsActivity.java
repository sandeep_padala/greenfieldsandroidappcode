package com.greenfields.homes.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.greenfields.homes.R;
import com.greenfields.homes.common.Utils;
import com.malinskiy.superrecyclerview.OnEmptyClickListener;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

public class AvailableRideDetailsActivity extends BaseActivity implements OnEmptyClickListener,OnMoreListener ,OnPhoneCallClickListener,OnDeleteRideListener{

    ArrayList<AvailableRideDetails> rideDetails=new ArrayList<AvailableRideDetails>();
    SuperRecyclerView recyclerView;
    private AvailableRidesListAdapter listAdapater;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private boolean isDataLoaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_ride_details);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("ridedetails");
        initViews();
        showAvailableRides();
    }

    private void initViews() {
        recyclerView=findViewById(R.id.rides_list);
        LinearLayoutManager layoutManager = new LinearLayoutManagerWrapper(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnMoreListener(this);
        recyclerView.setEmptyListener(this);
    }

    private void showAvailableRides() {
         AvailableRideDetails details=new AvailableRideDetails();
         String todayDate=Utils.getTodayDate();
        mFirebaseDatabase.child(todayDate).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               if(!isDataLoaded)
               {
                   Log.e("Count " ,""+dataSnapshot.getChildrenCount());
                   for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                       AvailableRideDetails details = postSnapshot.getValue(AvailableRideDetails.class);
                       details.key=postSnapshot.getKey();
                       rideDetails.add(details);
                   }
                   setData();
                   isDataLoaded=true;


               }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: " ,databaseError.getMessage());
            }


        });

    }

    private void showEmptyView() {
        if(rideDetails.size()<1)
        { recyclerView.setEmptyAdapter("No rides posted today,please try after some time..!", false, 0);
        }

    }

    @Override
    public void onEmptyItemClick(int emptyId) {

    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

    }

    private void setData() {
        rideDetails=Utils.getFutureRides(rideDetails);
        listAdapater = new AvailableRidesListAdapter(this, rideDetails);
        recyclerView.setAdapter(listAdapater);
        showEmptyView();
        //listAdapater.refreshDuplicates();

    }

    @Override
    public void performCall(final String phone) {
        requestPermission(Manifest.permission.CALL_PHONE, 1, new PermissionCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onPermissionStatus(boolean isGranted) {
                if(isGranted)
                {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + phone));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Can't call with out grant this permission,Please grant", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onDeleteRide(String key, final int pos) {
        String todayDate= Utils.getTodayDate();
        Task<Void> task = mFirebaseDatabase.child(todayDate).child(key).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Ride deleted successfully..!", Toast.LENGTH_SHORT).show();
                listAdapater.removeItem(pos);
                showEmptyView();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed to delete ride..!", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
