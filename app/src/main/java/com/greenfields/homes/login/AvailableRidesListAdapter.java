package com.greenfields.homes.login;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.greenfields.homes.R;
import com.greenfields.homes.common.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by on 6/21/2017.
 */

public class AvailableRidesListAdapter extends RecyclerView.Adapter<AvailableRidesListAdapter.MyViewHolder> {

    private final String uid;
    public List<AvailableRideDetails> rides;
    private Context mContext;
    OnPhoneCallClickListener listener;
    OnDeleteRideListener deleteRideListener;


    public AvailableRidesListAdapter(Context context, List<AvailableRideDetails> rides) {
        mContext = context;
        this.rides = rides;
        listener= (OnPhoneCallClickListener) context;
        deleteRideListener= (OnDeleteRideListener) context;
        uid=FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_rides, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final AvailableRideDetails deatils = rides.get(position);
        holder.tvDisplayName.setText(deatils.DisplayName);
        holder.tvPick.setText("Pickup:" + deatils.Pick);
        holder.tvDrop.setText("Drop:" + deatils.Drop);
        holder.tvStartTime.setText("Start Time:" + deatils.startTime);
        if(!isselfRide(deatils))
        {
            holder.btnDelete.setVisibility(View.GONE);
        }
        else
        {
            holder.btnDelete.setVisibility(View.VISIBLE);
        }
        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null)
                {
                    listener.performCall(deatils.phone);
                }
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext,R.style.TimePickerTheme);
                alertDialogBuilder.setMessage("Are you sure, You wanted to delete ride?");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        deleteRideListener.onDeleteRide(deatils.key,position);
                                       }
                                });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
      /* if(Utils.isPastTime(deatils.startTime))
        {
            holder.parent.setVisibility(View.GONE);
            holder.parent.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }*/

    }

    private boolean isselfRide(AvailableRideDetails details) {
        return uid.equals(details.uid);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return rides.size();
    }

    public void addItems(ArrayList<AvailableRideDetails> trendsDetail) {
        for (int i = 0; i < trendsDetail.size(); i++) {
            rides.add(trendsDetail.get(i));
        }
        notifyDataSetChanged();
    }

    public void removeItem(int pos) {
        rides.remove(pos);
        notifyItemChanged(pos);
    }

    public void refreshDuplicates() {

        ArrayList<AvailableRideDetails> tempList=getTempList();
        for(int i=0;i<tempList.size();i++)
        {
            AvailableRideDetails details=rides.get(i);
            if(Utils.isPastTime(details.startTime))
            {
                rides.remove(i);
                notifyItemChanged(i);
            }
        }
    }

    private ArrayList<AvailableRideDetails> getTempList() {
        ArrayList<AvailableRideDetails> tmpList=new ArrayList<AvailableRideDetails>();
        for(int i=0;i<rides.size();i++)
        {
            tmpList.add(rides.get(i));
        }
        return tmpList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDisplayName;
        public TextView tvPick;
        public TextView tvDrop;
        public TextView tvStartTime;
        public Button btnCall,btnDelete;
        public RelativeLayout parent;



        public MyViewHolder(View view) {
            super(view);
            tvDisplayName = (TextView) view.findViewById(R.id.tv_display_name);
            tvPick = (TextView) view.findViewById(R.id.tv_pick);
            tvDrop = (TextView) view.findViewById(R.id.tv_drop);
            tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
            btnCall=view.findViewById(R.id.btn_call);
            btnDelete=view.findViewById(R.id.btn_delete);
            parent=view.findViewById(R.id.parent);


        }
    }

    public void clearAdapter() {
        rides.clear();
    }

}