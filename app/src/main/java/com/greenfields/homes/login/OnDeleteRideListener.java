package com.greenfields.homes.login;

public interface OnDeleteRideListener {
    public void onDeleteRide(String key,int position);
}
