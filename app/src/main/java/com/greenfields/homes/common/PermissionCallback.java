package com.greenfields.homes.common;

/**
 * Created by kalyan pvs on 29-Jul-16.
 */
public interface PermissionCallback {

    void onPermissionStatus(boolean isGranted);

}
