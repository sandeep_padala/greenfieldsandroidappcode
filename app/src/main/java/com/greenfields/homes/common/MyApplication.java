package com.greenfields.homes.common;

import android.app.Application;
import android.content.Context;

/**
 * Created by sandeep padala on 1/23/2018.
 */

public class MyApplication extends Application {
    private static Context mApplicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationContext = getApplicationContext();
        Prefs.initPrefs(mApplicationContext);
       /* if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }*/

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/nexa-regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
    }

    public static Context getContext() {
        return mApplicationContext;
    }
}