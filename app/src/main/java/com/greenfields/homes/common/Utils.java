package com.greenfields.homes.common;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.greenfields.homes.login.AvailableRideDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Utils {
    public static String getTodayDate()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return  dateFormat.format(date);
    }

    public static String getSelectedTime(int hourOfDay, int minute) {

        int hours=hourOfDay;
        String amPm=" AM";
        if(hourOfDay==0)
        {
           hours=12;
           amPm=" AM";
        }
        else if(hourOfDay==12)
        {
            hours=hourOfDay;
            amPm=" PM";
        }
        else if(hourOfDay>12)
        {
            hours=hourOfDay-12;
            amPm=" PM";
        }
        return getAppendedValue(hours)+":"+getAppendedValue(minute) +amPm;
    }

    private static String getAppendedValue(int value) {
        if(value<10)
        {
            return "0"+value;
        }
        return ""+value;
    }

    public static boolean isOnline(Context context) {
        try {
            if (context == null) {
                context = MyApplication.getContext();
            }
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void showCrototn(Activity ctx, String message, Style style) {
        hideKeyboard(ctx);
        Snackbar snackBarView = Snackbar.make(ctx.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        TextView snackbarTextView = (TextView) snackBarView.getView().findViewById(android.support.design.R.id.snackbar_text);
        snackbarTextView.setMaxLines(3);
        int red = 0xfff44336;
        int green = 0xff4caf50;
        int blue = 0xff2195f3;
        int orange = 0xffffc107;
        if (style == Style.ALERT) {
            snackBarView.getView().setBackgroundColor(red);
        } else if (style == Style.CONFIRM) {
            snackBarView.getView().setBackgroundColor(green);
        } else if (style == Style.WARNING) {
            snackBarView.getView().setBackgroundColor(orange);
        } else if (style == Style.INFO) {
            snackBarView.getView().setBackgroundColor(blue);
        }
        snackBarView.setActionTextColor(Color.WHITE);
        snackBarView.show();

    }


    public static void hideKeyboard(Activity ctx) {
        // Check if no view has focus:
        try {
            View view = ctx.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                //inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isPastTime(String selectedTime) {

        String hours=selectedTime.split(":")[0];
        String minutes=selectedTime.split(":")[1].split(" ")[0];
        String amPm=selectedTime.split(":")[1].split(" ")[1];
        int selectedHour = Integer.parseInt(hours);
        int selectedMinute = Integer.parseInt(minutes);

        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentMin = rightNow.get(Calendar.MINUTE);;

        if(amPm.equals("PM"))
        {
            if(selectedHour!=12)
                selectedHour=selectedHour+12;
        }
        else
        {
            if(selectedHour==12)
            {
                selectedHour=0;
            }
        }
        if(currentHour==selectedHour) {
            if (selectedMinute < currentMin)
                return true;
        }
        if(selectedHour<currentHour)
        {
            return true;
        }
        return false;

    }

    public static ArrayList<AvailableRideDetails> getFutureRides(ArrayList<AvailableRideDetails> rides) {
        ArrayList<AvailableRideDetails> tmpList=new ArrayList<AvailableRideDetails>();
        for(int i=0;i<rides.size();i++)
        {
            if(!Utils.isPastTime(rides.get(i).startTime))
            tmpList.add(rides.get(i));
        }
        return tmpList;
    }
}
